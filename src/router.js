import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import store from "@/store";
Vue.use(Router);

const router = new Router({
  mode: "history",
  linkExactActiveClass: "vue-school-active-class",
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      const position = {};
      if (to.hash) {
        position.selector = to.hash;
        if (to.hash === "#experience") {
          position.offset = { y: 140 };
        }
        if (document.querySelector(to.hash)) {
          return position;
        }

        return false;
      }
    }
  },
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
      props: true
    },
    {
      path: "/todo/:idList/:id",
      name: "Todo",
      /*meta: { requiresAuth: true }*/
      props: true,
      component: () =>
        import(/* webpackChunkName: "DestinationDetails"*/ "./views/Todo"),
      beforeEnter: (to, from, next) => {
        const exists = store.todos.find(
          todo => todo.idList === to.params.idList
        );
        if (exists) {
          next();
        } else {
          next({ name: "notFound" });
        }
      }
    },
    {
      path: "/todo/:idList",
      name: "TodoList",
      /*meta: { requiresAuth: true }*/
      props: true,
      component: () =>
        import(/* webpackChunkName: "DestinationDetails"*/ "./views/TodoList"),
      beforeEnter: (to, from, next) => {
        const exists = store.todos.find(
          todo => todo.idList === to.params.idList
        );
        if (exists) {
          next();
        } else {
          next({ name: "notFound" });
        }
      }
    },
    {
      path: "/user",
      name: "user",
      component: () =>
        import(/* webpackChunkName: "User" */ "./views/User.vue"),
      /*meta: { requiresAuth: true }*/
    },
    {
      path: "/login",
      name: "login",
      component: () =>
        import(/* webpackChunkName: "Login" */
        "./views/Login.vue")
    },

    {
      path: "/404",
      alias: "*",
      name: "notFound",
      component: () =>
        import(/* webpackChunkName: "NotFound" */
        "./views/NotFound")
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.user) {
      next({
        name: "login",
        query: { redirect: to.fullPath }
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
